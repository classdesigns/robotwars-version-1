﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RobotWars;
using RobotWars_Tests.Factories;

namespace InitialisationStringToCommandParametersDeterminator_Tests
{
    [TestFixture]
    public class Constructor
    {
        [Test]
        public void TakesStringArgument()
        {
            InitialisationStringToCommandParametersDeterminator determinator = new InitialisationStringToCommandParametersDeterminator(InitialisationStringFactory.GetInputString());
            
            Assert.That(determinator, Is.Not.Null);
        }
    }

    [TestFixture]
    public class GetArenaSize
    {
        private readonly InitialisationStringToCommandParametersDeterminatorTestFactory _initialisationStringToCommandParametersDeterminatorTestFactory = new InitialisationStringToCommandParametersDeterminatorTestFactory();

        [Test]
        public void ReturnsTypeOf_XYCoordinates()
        {
            InitialisationStringToCommandParametersDeterminator determinator =
                new InitialisationStringToCommandParametersDeterminator(InitialisationStringFactory.GetInputString());

            var returnValue = determinator.GetArenaSize() ;
            
            Assert.That(returnValue, Is.TypeOf(typeof(XYCoordinates)));
        }

        [Test]
        public void ReturnsXof5AndYof5_GivenTheStandardInputString()
        {
            XYCoordinates expectedXY = new XYCoordinates(5, 5);
            var determinator = InitialisationStringToCommandParametersDeterminatorTestFactory.GetDeterminator();
            
            var actualReturnedXYCoordinates = determinator.GetArenaSize();
            
            Assert.That(actualReturnedXYCoordinates, Is.EqualTo(expectedXY));
        }
    }

    [TestFixture]
    public class GetRobotInitialisationCommands
    {
        private IInitialisationStringToCommandParametersDeterminator _initialisationStringToCommandParametersDeterminatorDeterminator;

        [SetUp]
        public void Setup()
        {
            _initialisationStringToCommandParametersDeterminatorDeterminator =
                InitialisationStringToCommandParametersDeterminatorTestFactory.GetDeterminator();
        }

        [Test]
        public void ReturnsListOfType_ListOfTypeString()
        {
            var returnValue = _initialisationStringToCommandParametersDeterminatorDeterminator.GetRobotInitialisationCommands();
            
            Assert.That(returnValue, Is.TypeOf(typeof(List<List<string>>)));
        }

        [Test]
        public void FirstItemOfFirstCommandReturned_Is1space2spaceN()
        {
            const string expected1space2spaceN = "1 2 N";
            var allRobotCommands = _initialisationStringToCommandParametersDeterminatorDeterminator.GetRobotInitialisationCommands();
            
            string firstItemOfFirstCommand = allRobotCommands.First().First();
            
            Assert.That(firstItemOfFirstCommand, Is.EqualTo(expected1space2spaceN));
        }

        [Test]
        public void SecondItemOfFirstCommandReturned_Is_LMLMLMLMM()
        {
            const string expectedLMLMLMLMM = "LMLMLMLMM";
            var allRobotCommands = _initialisationStringToCommandParametersDeterminatorDeterminator.GetRobotInitialisationCommands();
            
            var secondItemOfFirstCommand = allRobotCommands.First().Skip(1).First();
            
            Assert.That(secondItemOfFirstCommand, Is.EqualTo(expectedLMLMLMLMM));
        }

        [Test]
        public void FirstItemOfSecondCommandReturned_Is3space3spaceE()
        {
            const string expected3space3spaceE = "3 3 E";
            var allRobotCommands =
                _initialisationStringToCommandParametersDeterminatorDeterminator.GetRobotInitialisationCommands();

            var firstItemOfSecondCommand = allRobotCommands.Skip(1).First().First();
            
            Assert.That(firstItemOfSecondCommand, Is.EqualTo(expected3space3spaceE));
        }

        [Test]
        public void SecondItemOfSecondCommandReturned_Is_MMRMMRMRRM()
        {
            const string expectedMMRMMRMRRM = "MMRMMRMRRM";
            var allRobotCommands =
                _initialisationStringToCommandParametersDeterminatorDeterminator.GetRobotInitialisationCommands();

            
            var secondItemOfSecondCommand = allRobotCommands.Skip(1).First().Skip(1).First();
            
            Assert.That(secondItemOfSecondCommand, Is.EqualTo(expectedMMRMMRMRRM));
        }
    }

    [TestFixture]
    public class GetRobotMovementCommands
    {
        private IInitialisationStringToCommandParametersDeterminator _initialisationStringToCommandParametersDeterminatorDeterminator;

        [SetUp]
        public void Setup()
        {
            _initialisationStringToCommandParametersDeterminatorDeterminator =
                InitialisationStringToCommandParametersDeterminatorTestFactory.GetDeterminator();
        }

        [Test]
        public void ReturnsIEnumerableOfTypeString()
        {
            var actualValueReturned =
                _initialisationStringToCommandParametersDeterminatorDeterminator.GetRobotMovementCommands();

            Assert.That(actualValueReturned, Is.InstanceOf<IEnumerable<string>>());
        }

        [Test]
        public void Returns2Commands_When2RobotsAreInCommandInitialisationString()
        {
            const int expectedCountOf2 = 2;
            
            var actualNumberReturned = _initialisationStringToCommandParametersDeterminatorDeterminator.GetRobotMovementCommands().ToList().Count;
            
            Assert.That(actualNumberReturned, Is.EqualTo(expectedCountOf2));
        }

        [Test]
        public void FirstItemInListHasValue_LMLMLMLMM_WhenLMLMLMLMMisFirstMovementCommandInItitialisationString()
        {
            const string expectedLMLMLMLMM = "LMLMLMLMM";

            var actualValueOfFirstRobotMovementCommand = _initialisationStringToCommandParametersDeterminatorDeterminator.GetRobotMovementCommands().First();
            
            Assert.That(actualValueOfFirstRobotMovementCommand, Is.EqualTo(expectedLMLMLMLMM));
        }

        [Test]
        public void SecondItemInListHasValue_MMRMMRMRRM_WhenMMRMMRMRRMisSecondMovementCommandInInitialisationString()
        {
            const string expectedMMRMMRMRRM = "MMRMMRMRRM";

            var actualValueOfSecondRobotmovementCommand =
                _initialisationStringToCommandParametersDeterminatorDeterminator.GetRobotMovementCommands()
                                                                                .Skip(1)
                                                                                .First();

            Assert.That(actualValueOfSecondRobotmovementCommand, Is.EqualTo(expectedMMRMMRMRRM));
        }
    }
}
/*
Test Input:
5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM
*/