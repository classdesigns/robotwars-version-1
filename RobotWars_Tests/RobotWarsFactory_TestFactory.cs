﻿using RobotWars;
using RobotWars_Tests.Factories;

namespace RobotWarsFactory_Tests
{
    public class RobotWarsFactory_TestFactory
    {
        public static RobotWarsFactory GetRobotWarsFactory()
        {
            RobotWarsFactory robotWarsFactory =
                new RobotWarsFactory(InitialisationStringToCommandParametersDeterminatorTestFactory.GetDeterminator());
            return robotWarsFactory;
        }
    }
}