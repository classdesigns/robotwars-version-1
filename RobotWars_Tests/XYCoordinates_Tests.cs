﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RobotWars;

namespace XYCoordinates_Tests
{
    [TestFixture]
    public class Equals
    {
        [Test]
        public void ReturnsFalse_WhenDifferentObjectTypePassedIn()
        {
            const bool expectedEqualityOfFalse = false;
            XYCoordinates testXYCoords = new XYCoordinates(0, 0);
            
            var actualEqualityReturned = testXYCoords.Equals(new object());
            
            Assert.That(actualEqualityReturned, Is.EqualTo(expectedEqualityOfFalse));
        }

        [Test]
        public void ReturnsFalse_WhenNullIsPassedIn()
        {
            const bool expectedEqualityOfFalse = false;
            XYCoordinates testXYCoords = new XYCoordinates(0, 0);
            
            var actualEqualityValue = testXYCoords.Equals(null);
            
            Assert.That(actualEqualityValue, Is.EqualTo(expectedEqualityOfFalse));
        }

        [Test]
        public void ReturnsTrue_WhenBothCoordinatesXYValuesAre0()
        {
            const bool expectedEqualityOfTrue = true;

            XYCoordinates xyCoords1 = new XYCoordinates(0, 0);
            XYCoordinates xyCoords2 = new XYCoordinates(0, 0);
            
            var actualEqualityValue = xyCoords1.Equals(xyCoords2);
            
            Assert.That(actualEqualityValue, Is.EqualTo(expectedEqualityOfTrue));
        }

        [Test]
        public void ReturnsFalse_WhenXsAreTheSame_ButYsAreDifferent()
        {
            const bool expectedEqualityOfFalse = false;
            const int x = 5;
            XYCoordinates xyCoords1 = new XYCoordinates(x, 6);
            XYCoordinates xyCoords2 = new XYCoordinates(x, 7);
            
            bool actualEqualityValue = xyCoords1.Equals(xyCoords2);

            Assert.That(actualEqualityValue, Is.EqualTo(expectedEqualityOfFalse));
        }

        [Test]
        public void ReturnsTrue_WhenXsAre6AndYsAre7()
        {
            const bool expectedEqualityOfTrue = true;
            const int x = 6;
            const int y = 7;
            XYCoordinates xyCoords1 = new XYCoordinates(x, y);
            XYCoordinates xyCoords2 = new XYCoordinates(x, y);
            
            var actualEquality = xyCoords1.Equals(xyCoords2);
            
            Assert.That(actualEquality, Is.EqualTo(expectedEqualityOfTrue));
        }
    }
}
