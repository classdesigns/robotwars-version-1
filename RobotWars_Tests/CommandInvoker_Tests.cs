﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NSubstitute.Experimental;
using NUnit.Framework;
using RobotWars;
using RobotWars.enums;
using RobotWars_Tests.Factories;
using Robot_Tests;

namespace CommandInvoker_Tests
{
    [TestFixture]
    public class Constructor
    {
        [Test]
        public void Takes_IRobot_AsArgument()
        {
            Robot robot = RobotTestFactory.GetRobot(Direction.East);
            
            ICommandInvoker commandInvoker = new CommandInvoker(robot);
            
            Assert.That(commandInvoker, Is.Not.Null);
        }
    }

    [TestFixture]
    public class ExecuteCommands
    {
        private readonly CommandInvokerFactory _commandInvokerFactory = new CommandInvokerFactory();

        [Test]
        public void TakesString_AsArgument()
        {
            ICommandInvoker invoker = new CommandInvoker(RobotTestFactory.GetRobot(Direction.East));

            invoker.ExecuteCommands("LLMRR");

            Assert.That(invoker, Is.Not.Null);
        }

        [Test]
        public void InvokesRobotsLeftMethod_WhenLIsFirstLetter()
        {
            var robot = RobotTestFactory.GetFakeRobot();
            ICommandInvoker invoker = new CommandInvoker(robot);
            
            invoker.ExecuteCommands("L");
            
            robot.Received().Left();
        }

        [Test]
        public void InvokesRobotsRightMethod_WhenRIsFirstLetter()
        {
            IRobot robot = RobotTestFactory.GetFakeRobot();
            ICommandInvoker invoker = CommandInvokerFactory.GetCommandInvoker(robot);
            
            invoker.ExecuteCommands("R");
            
            robot.Received().Right();
        }

        [Test]
        public void InvokesRobotsMoveMethod_WhenMIsFirstLetter()
        {
            IRobot robot = RobotTestFactory.GetFakeRobot();
            ICommandInvoker invoker = CommandInvokerFactory.GetCommandInvoker(robot);

            invoker.ExecuteCommands("M");

            robot.Received().Move();
        }

        [Test]
        public void InvokesRobotsLeftLeftMoveMethods_WhenLLMCommandIsParsed()
        {
            IRobot robot = RobotTestFactory.GetFakeRobot();
            ICommandInvoker invoker = CommandInvokerFactory.GetCommandInvoker(robot);

            invoker.ExecuteCommands("LLM");

            Received.InOrder(() => 
                {robot.Left();
                    robot.Left();
                    robot.Move();
                });
        }
    }
}
