﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using RobotWars;
using RobotWars_Tests.Factories;

namespace RobotWarsFactory_Tests
{
    [TestFixture]
    public class Constructor
    {
        [Test]
        public void Takes_IInitialisationStringToCommandParametersDeterminator()
        {
            IInitialisationStringToCommandParametersDeterminator initialisationStringToCommandParametersDeterminator =
                new InitialisationStringToCommandParametersDeterminator(InitialisationStringFactory.GetInputString());
            
            IRobotWarsFactory robotWarsFactory = new RobotWarsFactory(initialisationStringToCommandParametersDeterminator);
            
            Assert.That(robotWarsFactory, Is.Not.Null);
        }
    }

    [TestFixture]
    public class GetArena
    {
        
        [Test]
        public void ReturnsIArenaOfSize5By5_WhenFirstLineOfInputString_Is5Space5()
        {
            const int expectedValueOf5 = 5;
            var robotWarsFactory = RobotWarsFactory_TestFactory.GetRobotWarsFactory();


            IArena arena = robotWarsFactory.GetArena();

            Assert.That(arena.Y, Is.EqualTo(expectedValueOf5));
            Assert.That(arena.X, Is.EqualTo(expectedValueOf5));
        }
    }

    [TestFixture]
    public class GetRobots
    {
        private RobotWarsFactory _robotWarsFactory;

        [TestFixtureSetUp]
        public void Setup()
        {
            _robotWarsFactory = RobotWarsFactory_TestFactory.GetRobotWarsFactory();
        }

        [Test]
        public void ReturnsList_OfTypeRobot()
        {
            var robots = _robotWarsFactory.GetRobots();

            Assert.That(robots, Is.TypeOf<List<IRobot>>());
        }

        [Test]
        public void Returns2Robots_WhenGivenStandardInputCommands()
        {
            const int expectedNumberOf2 = 2;

            int actualNumber = _robotWarsFactory.GetRobots().Count;

            Assert.That(actualNumber, Is.EqualTo(expectedNumberOf2));
        }

        [Test]
        public void FirstRobotsPosition_Is12N()
        {
            const string expected12N = "1 2 N";
            
            var actualPosition = _robotWarsFactory.GetRobots().First().GetCurrentPosition();
            
            Assert.That(actualPosition, Is.EqualTo(expected12N));
        }

        [Test]
        public void SecondRobotsPosition_Is33E()
        {
            const string expected33E = "3 3 E";
            
            var actualPosition = _robotWarsFactory.GetRobots().Skip(1).First().GetCurrentPosition();

            Assert.That(actualPosition, Is.EqualTo(expected33E));
        }
    }

    [TestFixture]
    public class GetRobotMovementCommands
    {
        private IRobotWarsFactory _robotWarsFactory;

        [TestFixtureSetUp]
        public void Setup()
        {
            _robotWarsFactory = RobotWarsFactory_TestFactory.GetRobotWarsFactory();
        }

        [Test]
        public void IsTypeOf_IEnumerableString()
        {
            var valueReturnedFromGetCommands = _robotWarsFactory.GetRobotMovementCommands();
            
            Assert.That(valueReturnedFromGetCommands, Is.InstanceOf<IEnumerable<string>>());
        }

        [Test]
        public void IsTypeOf_ListOfIEnumerableString()
        {
            var valueReturnedFromGetCommands = _robotWarsFactory.GetRobotMovementCommands();

            Assert.That(valueReturnedFromGetCommands, Is.InstanceOf<IEnumerable<string>>());
        }

        [Test]
        public void Has2ItemsInList_When2RobotCommandsAreInInitiaslisationString()
        {
            const int expectedNumberOfRobotCommands2 = 2;
            
            var actualNumberOfRobotCommands = _robotWarsFactory.GetRobotMovementCommands().ToList().Count;

            Assert.That(actualNumberOfRobotCommands, Is.EqualTo(expectedNumberOfRobotCommands2));
        }
    }
}

/*
 Test Input:
5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM
 */