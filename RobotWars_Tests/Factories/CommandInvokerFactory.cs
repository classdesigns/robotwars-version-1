﻿using RobotWars;

namespace RobotWars_Tests.Factories
{
    public class CommandInvokerFactory
    {
        public static ICommandInvoker GetCommandInvoker(IRobot robot)
        {
            return new CommandInvoker(robot);
        }
    }
}