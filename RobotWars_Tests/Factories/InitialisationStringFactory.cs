﻿using System.Text;

namespace RobotWars_Tests.Factories
{
    public class InitialisationStringFactory
    {
        public static string GetInputString()
        {
            StringBuilder inputBuilder = new StringBuilder();
            inputBuilder.AppendLine("5 5");
            inputBuilder.AppendLine("1 2 N");
            inputBuilder.AppendLine("LMLMLMLMM");
            inputBuilder.AppendLine("3 3 E");
            inputBuilder.Append("MMRMMRMRRM");

            return inputBuilder.ToString();
        }
    }
}