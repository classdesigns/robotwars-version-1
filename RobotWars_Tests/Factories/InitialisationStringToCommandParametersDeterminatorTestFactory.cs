﻿using RobotWars;

namespace RobotWars_Tests.Factories
{
    public class InitialisationStringToCommandParametersDeterminatorTestFactory
    {
        public static IInitialisationStringToCommandParametersDeterminator GetDeterminator()
        {
            return new InitialisationStringToCommandParametersDeterminator(InitialisationStringFactory.GetInputString());
        }
    }
}