﻿using NSubstitute;
using RobotWars;
using RobotWars.enums;

namespace RobotWars_Tests.Factories
{
    public class RobotTestFactory
    {
        public static Robot GetRobot(Direction startDirection)
        {
            return new Robot(new XYCoordinates(0, 0), startDirection);
        }

        public static IRobot GetFakeRobot()
        {
            IRobot robot = Substitute.For<IRobot>();
            return robot;
        }

        public static StubRobot GetStubRobot(Direction startDirection)
        {
            return new StubRobot(0, 0, startDirection);
        }

        public static IRobot GetRobotWithSpecifiedPositionAndDirection(int x, int y, Direction startDirection)
        {
            return new Robot(new XYCoordinates(x, y), startDirection);
        }
    }

    public class StubRobot : Robot
    {
        public StubRobot(int x, int y, Direction startDirection) : base(new XYCoordinates(x, y), startDirection)
        {}
        
        public void SetCurrentPosition(int x, int y)
        {
            CurrentXPosition = x;
            CurrentYPosition = y;
        }
    }
}