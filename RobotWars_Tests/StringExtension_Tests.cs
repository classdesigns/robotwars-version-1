﻿using RobotWars;
using NUnit.Framework;

namespace StringExtension_Tests
{
    [TestFixture]
    public class SanitiseDoubleEscapedCrLf
    {
        [Test]
        public void ReplacesSlashSlashRSlashSLashN_WithSlashRSlashN()
        {
            const string expectedSanitisedValue = "\r\n";
            const string slashSlashRSlashSlashN = "\\r\\n";
            
            string actualSanitisedValue = slashSlashRSlashSlashN.SanitiseDoubleEscapedCrLf();
            
            Assert.That(actualSanitisedValue, Is.EqualTo(expectedSanitisedValue));
        }
    }
}
