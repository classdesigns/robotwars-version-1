﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RobotWars;
using RobotWars.enums;
using RobotWars_Tests.Factories;

namespace Robot_Tests
{
    [TestFixture]
    public class Constructor
    {
        [Test]
        public void TakesXYCoordinateAndDirection_AsArgument()
        {
            XYCoordinates coordinates = new XYCoordinates(5, 5);
            Robot robot = new Robot(coordinates, Direction.North);
            
            Assert.That(robot, Is.Not.Null);
        }

        [Test]
        public void SetsCurrentXPosition_ToValueOfxStartPosition()
        {
            int expectedValueOf5 = 5;
            Robot robot = new Robot(new XYCoordinates(5, 5), Direction.East);
            
            var actualXPosition = robot.CurrentXPosition;
            
            Assert.That(actualXPosition, Is.EqualTo(expectedValueOf5));
        }

        [Test]
        public void SetsCurrentYPosition_ToValueOfyStartPosition()
        {
            const int expectedValueOf4 = 4;
            Robot robot = new Robot(new XYCoordinates(4, 4), Direction.South);
            
            var actualYPosition = robot.CurrentYPosition;
            
            Assert.That(actualYPosition, Is.EqualTo(expectedValueOf4));
        }

        [Test]
        public void SetsCurrentDirection_ToValueOfstartDirection()
        {
            Direction expectedDirectionOfWest = Direction.West;
            Robot robot = new Robot(new XYCoordinates(0, 0), Direction.West);
           
            var actualDirection = robot.CurrentDirection;
            
            Assert.That(actualDirection, Is.EqualTo(expectedDirectionOfWest));
        }
    }

    [TestFixture]
    public class Move
    {
        [Test]
        public void IncrementsCurrentYPosition_WhenFacingNorth()
        {
            const int expectedValueOf1 = 1;
            Robot robot = new Robot(new XYCoordinates(0, 0), Direction.North);

            robot.Move();
            var actualCurrentYValue = robot.CurrentYPosition;
            
            Assert.That(actualCurrentYValue, Is.EqualTo(expectedValueOf1));
        }

        [Test]
        public void IncrementsCurrentXPosition_WhenFacingEast()
        {
            const int expectedValueOf2 = 2;
            Robot robot = new Robot(new XYCoordinates(1, 0), Direction.East);
            
            robot.Move();
            var actualCurrentXPosition = robot.CurrentXPosition;
            
            Assert.That(actualCurrentXPosition, Is.EqualTo(expectedValueOf2));
        }

        [Test]
        public void DecrementsCurrentYPosition_WhenFacingSouth()
        {
            const int expectedValueOf5 = 5;
            Robot robot = new Robot(new XYCoordinates(0, 6), Direction.South);

            robot.Move();
            var actualCurrentYPosition = robot.CurrentYPosition;
            
            Assert.That(actualCurrentYPosition, Is.EqualTo(expectedValueOf5));
        }

        [Test]
        public void DecrementsCurrentXPosition_WhenFacingWest()
        {
            const int expectedValueOf4 = 4;
            Robot robot = new Robot(new XYCoordinates(5, 0), Direction.West);
            
            robot.Move();

            Assert.That(robot.CurrentXPosition, Is.EqualTo(expectedValueOf4));
        }
    }

    [TestFixture]
    public class Left
    {
        [TestCase(Direction.North, Direction.West)]
        [TestCase(Direction.West, Direction.South)]
        [TestCase(Direction.South, Direction.East)]
        [TestCase(Direction.East, Direction.North)]
        public void ChangesCurrentDirection_AntiClockwise_ForEachCardinalPoint(Direction startDirection, Direction expectedFinalDirection)
        {
            Robot robot = RobotTestFactory.GetRobot(startDirection);
            
            robot.Left();

            Assert.That(robot.CurrentDirection, Is.EqualTo(expectedFinalDirection));
        }
    }

    [TestFixture]
    public class Right
    {
        [TestCase(Direction.North, Direction.East)]
        [TestCase(Direction.East, Direction.South)]
        [TestCase(Direction.South, Direction.West)]
        [TestCase(Direction.West, Direction.North)]
        public void ChangesCurrentDirection_Clockwise_ForEachCardinalPoint(Direction startDirection, Direction expectedFinalDirection)
        {
            Robot robot = RobotTestFactory.GetRobot(startDirection);

            robot.Right();

            Assert.That(robot.CurrentDirection, Is.EqualTo(expectedFinalDirection));
        }
    }

    [TestFixture]
    public class GetCurrentPosition
    {
        [Test]
        public void Returns_OneSpaceThreeSpaceN_WhenXCoordinateIs1YCoordinateIs2AndDirectionIsNorth()
        {
            const string expected1Space3SpaceN = "1 3 N";
            StubRobot robot = RobotTestFactory.GetStubRobot(Direction.North);
            robot.SetCurrentPosition(1, 3);

            string currentPosition = robot.GetCurrentPosition();

            Assert.That(currentPosition, Is.EqualTo(expected1Space3SpaceN));
        }

        [Test]
        public void Returns_5Space1SpaceE_WhenXCoordinateIs5YCoordinateIs1AndDirectionIsEast()
        {
            const string expected5Space1SpaceN = "5 1 E";
            StubRobot robot = RobotTestFactory.GetStubRobot(Direction.East);
            robot.SetCurrentPosition(5, 1);
            
            string currentDirection = robot.GetCurrentPosition();
            
            Assert.That(currentDirection, Is.EqualTo(expected5Space1SpaceN));
        }
    }
}
