﻿using System.Collections.Generic;
using System.Linq;

namespace RobotWars
{
    public class InitialisationStringToCommandParametersDeterminator : IInitialisationStringToCommandParametersDeterminator
    {
        private readonly string _robotWarsInputString;
        private readonly int _numberOfRobots;

        public InitialisationStringToCommandParametersDeterminator(string robotWarsInputString)
        {
            _robotWarsInputString = robotWarsInputString;
            _numberOfRobots = CalculateNumRobotsFromInputCommand();
        }

        public XYCoordinates GetArenaSize()
        {
            string firstLine = GetFirstLine();
            int x = int.Parse(StringExtensions.SplitBySpace(firstLine).First());
            int y = int.Parse(StringExtensions.SplitBySpace(firstLine).Skip(1).First());

            return new XYCoordinates(x, y);
        }

        public List<List<string>> GetRobotInitialisationCommands()
        {
            var robotInitialisationCommands = new List<List<string>>(_numberOfRobots);

            for (int robotNumber = 0; robotNumber++ < _numberOfRobots; )
            {
                robotInitialisationCommands.Add(GetRequiredRobotCommands(robotNumber).ToList());
            }

            return robotInitialisationCommands;
        }

/*
        public List<string> GetRobotMovementCommands()
        {
            List<string> robotMovementCommands = new List<string>(_numberOfRobots);
            for (int robotNumber = 0; robotNumber++ < _numberOfRobots; )
            {
                robotMovementCommands.Add(GetRequiredRobotMovementCommands(robotNumber));
            }

            return robotMovementCommands;
        }
*/
        public IEnumerable<string> GetRobotMovementCommands()
        {
//            List<string> robotMovementCommands = new List<string>(_numberOfRobots);
            for (int robotNumber = 0; robotNumber++ < _numberOfRobots; )
            {
                yield return GetRequiredRobotMovementCommands(robotNumber);
            }

//            return robotMovementCommands;
        }

        private string GetRequiredRobotMovementCommands(int robotNumber)
        {
            const int offsetBetweenEachMovementCommand = 2;
            string movementCommand =
                _robotWarsInputString.GetInputCommandsSplitByCrLf().Skip(offsetBetweenEachMovementCommand * robotNumber).First();

            return movementCommand;
        }
        /*
        Test Input:
        5 5
        1 2 N
        LMLMLMLMM
        3 3 E
        MMRMMRMRRM
        */
        private int CalculateNumRobotsFromInputCommand()
        {
            /*return (GetInputCommandsSplitByCrLf(_robotWarsInputString).Count() -1) / 2;*/
            return (_robotWarsInputString.GetInputCommandsSplitByCrLf().Count() - 1) / 2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="requiredRobotOrdinal">1 based ordinal NOT 0</param>
        /// <returns></returns>
        private IEnumerable<string> GetRequiredRobotCommands(int requiredRobotOrdinal)
        {
            const int numberOfLinesInACompleteRobotCommand = 2;

            return _robotWarsInputString.GetInputCommandsSplitByCrLf().Skip(DetermineHowManyLinesToSkipToGetGivenRobotsCommand(requiredRobotOrdinal, numberOfLinesInACompleteRobotCommand)).Take(2);
        }

        private static int DetermineHowManyLinesToSkipToGetGivenRobotsCommand(int requiredRobotOrdinal, int numberOfLinesInACompleteRobotCommand)
        {
            return (requiredRobotOrdinal * numberOfLinesInACompleteRobotCommand) -1;
        }

        private string GetFirstLine()
        {
            return _robotWarsInputString.GetInputCommandsSplitByCrLf().First();
        }

        
    }
}
/*
Test Input:
5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM
*/