﻿using System;
using System.Collections.Generic;

namespace RobotWars
{
    public interface ICommandInvoker
    {
        void ExecuteCommands(string commands);
    }

    public class CommandInvoker : ICommandInvoker
    {
        private readonly IRobot _robot;
        private readonly Dictionary<char, Action> _commandToActions = new Dictionary<char, Action>();

        public CommandInvoker(IRobot robot)
        {
            _robot = robot;
            ConfigureCommandActions();
        }

        private void ConfigureCommandActions()
        {
            _commandToActions.Add('L', _robot.Left);
            _commandToActions.Add('R', _robot.Right);
            _commandToActions.Add('M', _robot.Move);
        }

        public void ExecuteCommands(string commands)
        {
            foreach (var command in commands)
            {
                _commandToActions[command].Invoke();
            }          
        }
    }
}