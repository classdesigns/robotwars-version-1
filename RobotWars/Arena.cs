﻿namespace RobotWars
{
    public class Arena : IArena
    {
        public Arena(int xSize, int ySize)
        {
            X = xSize;
            Y = ySize;
        }

        public int X { get; private set; }
        public int Y { get; private set; }
    }
}