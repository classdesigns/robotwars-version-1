﻿namespace RobotWars
{
    public interface IArena
    {
        int X { get; }
        int Y { get; }
    }
}