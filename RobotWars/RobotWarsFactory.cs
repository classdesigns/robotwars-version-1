﻿using System;
using System.Collections.Generic;
using System.Linq;
using RobotWars.enums;

namespace RobotWars
{
    public class RobotWarsFactory : IRobotWarsFactory
    {
        private readonly IInitialisationStringToCommandParametersDeterminator _commandStringToParametersDeterminator;

        public RobotWarsFactory(IInitialisationStringToCommandParametersDeterminator commandStringToParametersDeterminator)
        {
            _commandStringToParametersDeterminator = commandStringToParametersDeterminator;
            SetArenaSizes();
        }

        private void SetArenaSizes()
        {
            var arenaSize = _commandStringToParametersDeterminator.GetArenaSize();
            SpecifiedX = arenaSize.X;
            SpecifiedY = arenaSize.Y;
        }

        public IArena GetArena()
        {
            return new Arena(SpecifiedX, SpecifiedY);
        }

        public int SpecifiedY { get; private set; }

        public int SpecifiedX { get; private set; }

        public List<IRobot> GetRobots()
        {
            var robots = new List<IRobot>();
            foreach (var robotCommandList in _commandStringToParametersDeterminator.GetRobotInitialisationCommands())
            {
                robots.Add(GetRobot(robotCommandList));
            }

            return robots;
        }

        private Robot GetRobot(List<string> robotCommandList)
        {
            var firstCommandInList = robotCommandList.First().SplitBySpace().ToList();
            XYCoordinates coords = GetRobotStartCoordinates(firstCommandInList);
            Direction direction = GetRobotStartDirection(firstCommandInList);

            return new Robot(coords, direction);
        }

/*        public List<string> GetRobotMovementCommands()
        {
            return _commandStringToParametersDeterminator.GetRobotMovementCommands();
        }*/

        public IEnumerable<string> GetRobotMovementCommands()
        {
            return _commandStringToParametersDeterminator.GetRobotMovementCommands();
        }

        private Direction GetRobotStartDirection(IEnumerable<string> positionCommand)
        {
            char directionChar = positionCommand.Skip(2).First().First();

            return (Direction)Enum.ToObject(typeof(Direction), directionChar);
        }


        private XYCoordinates GetRobotStartCoordinates(IEnumerable<string> positionCommand)
        {
            var x = int.Parse(positionCommand.First());
            var y = int.Parse(positionCommand.Skip(1).First());

            return new XYCoordinates(x, y);
        }
    }
}