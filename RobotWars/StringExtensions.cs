﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace RobotWars
{
    public static class StringExtensions
    {
        private static string _crLf = Environment.NewLine;
        
        public static IEnumerable<string> GetInputCommandsSplitByCrLf(this string fullCommandString)
        {
            return Regex.Split(fullCommandString, _crLf);
        }

        public static IEnumerable<string> SplitBySpace(this string lineToSplit)
        {
            return lineToSplit.Split();
        }

        public static string SanitiseDoubleEscapedCrLf(this string valueToSanitise)
        {
            return valueToSanitise.Replace("\\r\\n", "\r\n");
        }
    }
}