﻿using System.Collections.Generic;

namespace RobotWars
{
    public interface IRobotWarsFactory
    {
        IArena GetArena();
        int SpecifiedY { get; }
        int SpecifiedX { get; }
        List<IRobot> GetRobots();
        IEnumerable<string> GetRobotMovementCommands();
    }
}