﻿namespace RobotWars
{
    public class XYCoordinates
    {
        public XYCoordinates(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            var comparisonObject = obj as XYCoordinates;
            if (comparisonObject == null)
                return false;

            return (DoXsCompare(comparisonObject) && DoYsCompare(comparisonObject));
        }

        private bool DoYsCompare(XYCoordinates comparisonObject)
        {
            return Y.Equals(comparisonObject.Y);
        }

        private bool DoXsCompare(XYCoordinates comparisonObject)
        {
            return X.Equals(comparisonObject.X);
        }

        public int X { get; set; }
        public int Y { get; set; }
    }
}