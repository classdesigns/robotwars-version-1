﻿using System.Collections.Generic;

namespace RobotWars
{
    public interface IInitialisationStringToCommandParametersDeterminator
    {
        XYCoordinates GetArenaSize();
        List<List<string>> GetRobotInitialisationCommands();
        IEnumerable<string> GetRobotMovementCommands();
    }
}