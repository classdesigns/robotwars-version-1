﻿using System;
using System.Collections.Generic;
using System.Text;
using RobotWars;
using RobotWars.enums;

namespace RobotWars
{
    public interface IRobot
    {
        void Move();
        void Left();
        void Right();
        int CurrentXPosition { get; }
        int CurrentYPosition { get; }
        Direction CurrentDirection { get; }
        string GetCurrentPosition();
    }

    public class Robot : IRobot
    {
        private Dictionary<Direction, Action> _moveCommandActions = new Dictionary<Direction, Action>();
        private Dictionary<Direction, Direction> _leftCommandActions = new Dictionary<Direction, Direction>();
        private Dictionary<Direction, Direction> _rightCommandActions = new Dictionary<Direction, Direction>();

        public Robot(XYCoordinates startCoordinates, Direction startDirection)
        {
            CurrentXPosition = startCoordinates.X;
            CurrentYPosition = startCoordinates.Y;
            CurrentDirection = startDirection;
            ConfigureActions();
        }

        public void Move()
        {
            _moveCommandActions[CurrentDirection].Invoke();
        }

        public void Left()
        {
            CurrentDirection = _leftCommandActions[CurrentDirection];
        }

        public void Right()
        {
            CurrentDirection = _rightCommandActions[CurrentDirection];
        }

        public string GetCurrentPosition()
        {
            const string space = " ";
            StringBuilder currentPosition = new StringBuilder();
            currentPosition.Append(CurrentXPosition + space + CurrentYPosition + space + GetCurrentDirectionMnemonic());

            return currentPosition.ToString();
        }

        private string GetCurrentDirectionMnemonic()
        {
            return Enum.GetName(typeof (Direction), CurrentDirection).Substring(0, 1);
        }

        public int CurrentXPosition { get; protected set; }

        public int CurrentYPosition { get; protected set; }

        public Direction CurrentDirection { get; protected set; }

        private void ConfigureActions()
        {
            ConfigureMovementActions();
            ConfigureLeftActions();
            ConfigureRightActions();
        }

        private void ConfigureRightActions()
        {
            _rightCommandActions.Add(Direction.North, Direction.East);
            _rightCommandActions.Add(Direction.East, Direction.South);
            _rightCommandActions.Add(Direction.South, Direction.West);
            _rightCommandActions.Add(Direction.West, Direction.North);
        }

        private void ConfigureMovementActions()
        {
            _moveCommandActions.Add(Direction.North, IncrementY);
            _moveCommandActions.Add(Direction.East, IncrementX);
            _moveCommandActions.Add(Direction.South, DecrementY);
            _moveCommandActions.Add(Direction.West, DecrementX);
        }

        private void ConfigureLeftActions()
        {
            _leftCommandActions.Add(Direction.North, Direction.West);
            _leftCommandActions.Add(Direction.West, Direction.South);
            _leftCommandActions.Add(Direction.South, Direction.East);
            _leftCommandActions.Add(Direction.East, Direction.North);
        }

        private void DecrementX()
        {
            CurrentXPosition--;
        }

        private void DecrementY()
        {
            CurrentYPosition--;
        }

        private void IncrementY()
        {
            CurrentYPosition++;
        }

        private void IncrementX()
        {
            CurrentXPosition++;
        }
    }
}