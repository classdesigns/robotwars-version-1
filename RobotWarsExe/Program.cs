﻿using System;
using System.Collections.Generic;
using System.Linq;
using RobotWars;

namespace RobotWarsExe
{
    class Program
    {
       
        static void Main(string[] args)
        {
            string robotWarsInputString = args[0].SanitiseDoubleEscapedCrLf();
            IInitialisationStringToCommandParametersDeterminator determinator = new InitialisationStringToCommandParametersDeterminator(robotWarsInputString);
            IRobotWarsFactory factory = new RobotWarsFactory(determinator);
            int robotCounter = 0;
            List<string> robotMovementCommands = factory.GetRobotMovementCommands().ToList();
               

            foreach (var robot in factory.GetRobots())
            {
                ICommandInvoker commandInvoker = new CommandInvoker(robot);
                commandInvoker.ExecuteCommands(robotMovementCommands.ElementAt(robotCounter));
                robotCounter++;
                
                Console.WriteLine(robot.GetCurrentPosition());
            }

            Console.ReadLine();
        }
    }
}
